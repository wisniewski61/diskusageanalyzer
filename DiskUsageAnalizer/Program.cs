﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace DiskUsageAnalizer
{
    class Program
    {
        class MessageHelper
        {
            public MessageHelper(string dir, string size)
            {
                DIR = dir;
                SIZE = size;
            }

            public string DIR;
            public string SIZE;
        }//MessageHelper

        static long GetDirectorySize(string parentDirectory)
        {
            return new DirectoryInfo(parentDirectory).GetFiles("*.*", SearchOption.AllDirectories).Sum(file => file.Length);
        }//GetDirectorySize()

        static string ConvertToStringWithUnits(long size)
        {
            double directorySize = size;
            string[] units = { "bytes", "KB", "MB", "GB", "TB" };
            int i = 0;
            while (directorySize >= 1024.0 && i < units.Length-1)
            {
                directorySize /= 1024.0;
                directorySize = Math.Round(directorySize, 2);
                i++;
            }//while
            return directorySize.ToString() + " " + units[i];
        }//ConvertToStringWithUnits()

        static void Display(List<MessageHelper> msgsList, int longestDirName)
        {
            foreach(MessageHelper msg in msgsList)
            {
                StringBuilder sb = new StringBuilder(msg.DIR);
                int difference = longestDirName - msg.DIR.Length;
                for (int i = 0; i < difference; i++)
                    sb.Append(" ");
                sb.Append(" ").Append(msg.SIZE);
                Console.WriteLine(sb.ToString());
            }//for
        }//Display()

        static void ClearLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth-1));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
        }//ClearLine()

        static void Analyze(string parentDirectory)
        {
            string[] directories;
            try
            {
                directories = Directory.GetDirectories(parentDirectory);
            }//try
            catch(DirectoryNotFoundException)
            {
                Console.WriteLine("Directory " + parentDirectory + " not found.");
                return;
            }//catch

            Console.WriteLine("Found " + directories.Length + " directories.");
            List<MessageHelper> msgs = new List<MessageHelper>();
            int longestDirName = 0;
            int counter = 1;
            foreach (string directory in directories)
            {
                ClearLine(); ClearLine();
                Console.WriteLine("Analyzing directory " + counter + " of " + directories.Length + ".");
                counter++;
                Console.WriteLine("Current directory: " + directory);

                try
                {
                    if (directory.Length > longestDirName)
                        longestDirName = directory.Length;
                    long size = GetDirectorySize(directory);
                    string sizeStr = ConvertToStringWithUnits(size);
                    MessageHelper mh = new MessageHelper(directory, sizeStr);
                    msgs.Add(mh);
                }//try
                catch (UnauthorizedAccessException)
                {
                    MessageHelper mh = new MessageHelper(directory, "Access denied");
                    msgs.Add(mh);
                }//catch
            }//for

            ClearLine(); ClearLine();
            Display(msgs, longestDirName);
        }//Analyze()

        static void Main(string[] args)
        {
            Console.WriteLine("Disk Usage Analyzer");
            Console.Write("Set directory: ");
            string directory = Console.ReadLine();

            Thread analyzeThread = new Thread(() => Analyze(directory));
            Console.WriteLine("\nAnalyze in progress. Please wait.\n\n");
            analyzeThread.Start();
            analyzeThread.Join();

            Console.WriteLine("\nAnalyze complete. Press any key to exit.");
            Console.ReadKey();
        }//Main()
    }
}
